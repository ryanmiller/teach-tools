﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;

public class Countdown : MonoBehaviour
{
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI timeText;
    public ParticleSystem particles;
    public Color almostDoneColor = Color.red;
    public Color normalColor = Color.green;
    private Camera mainCam;
    private Coroutine timer;
    
    public GameObject screenConfigure;
    public GameObject screenTimer;
    public TMP_InputField timeInputField;
    public TMP_InputField labelInputField;

    public void OnEnable()
    {
        mainCam = Camera.main;
        screenConfigure.SetActive(true);
        screenTimer.SetActive(false);
        mainCam.backgroundColor = normalColor;
    }

    public void StartTimer()
    {
        DateTime result;
        bool validDate = DateTime.TryParseExact(timeInputField.text, "m:ss", CultureInfo.CurrentCulture, DateTimeStyles.None, out result);
        if (validDate)
        {
            screenConfigure.SetActive(false);
            screenTimer.SetActive(true);
            mainCam.backgroundColor = normalColor;
            SetCountdownTitle(labelInputField.text);
            SetCountdownFor(result.Second, result.Minute);
        }
        else
        {
            FindObjectOfType<StudentData>().ShowWarning("The date you entered needs to be in the mm:ss format (eg: 01:30)");
        }
    }

    private void OnDisable()
    {
        if (timer != null)
        {
            StopCoroutine(timer);
        }
    }

    public void SetCountdownTitle(string title)
    {
        titleText.SetText(title);
    }
    
    public void SetCountdownFor(int seconds, int minutes = 0)
    {
        if (timer != null)
        {
            StopCoroutine(timer);
        }
        timer = StartCoroutine(BeginCountdown(seconds, minutes));
    }

    IEnumerator BeginCountdown(int seconds, int minutes = 0)
    {
        if (seconds > 60)
        {
            minutes = seconds / 60;
            seconds -= minutes * 60;
        }
        while (seconds >= 0 || minutes >= 0)
        {
            seconds--;
            if (minutes == 0)
            {
                mainCam.backgroundColor = almostDoneColor;
            }
            else
            {
                mainCam.backgroundColor = normalColor;
            }
            if (seconds <= 0)
            {
                if (minutes <= 0)
                {
                    timeText.SetText("00:00");
                    CountdownEnd();
                    yield break;
                }
                else
                {
                    timeText.SetText(minutes.ToString("00") + ":00");
                    yield return new WaitForSecondsRealtime(1);
                    minutes--;
                    seconds = 60;
                }
            }
            else
            {
                timeText.SetText(minutes.ToString("00") + ":" + seconds.ToString("00"));
                yield return new WaitForSecondsRealtime(1);
            }
        }
    }

    public void CountdownEnd()
    {
        particles.Play();
    }
}
