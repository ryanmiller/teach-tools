﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public GameObject[] screens;
    public GameObject defaultScreen;

    void Start()
    {
        OpenOnly(defaultScreen);
    }

    public void OpenDefault()
    {
        OpenOnly(defaultScreen);
    }

    public void OpenOnly(GameObject target)
    {
        foreach (GameObject g in screens)
        {
            if (g != target)
            {
                g.SetActive(false);
            }
            else
            {
                g.SetActive(true);
            }
        }
    }
}
