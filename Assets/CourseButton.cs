﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CourseButton : MonoBehaviour
{
    public TextMeshProUGUI courseName;
    public Course myData;

    public void Initialize(Course courseData)
    {
        myData = courseData;
        courseName.SetText(myData.className);
        gameObject.SetActive(true);
    }

    public void ViewCourse()
    {
        FindObjectOfType<StudentData>().ViewCourse(myData);
    }

    public void DeleteCourse()
    {
        FindObjectOfType<StudentData>().DeleteCourse(myData.className);
        FindObjectOfType<StudentData>().GetCourses();
    }
}
