﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using System.IO;
using UnityEngine;
using UnityEngine.Serialization;

public class RandomSelector : MonoBehaviour
{
    private StudentData studentData;
    private List<Person> personInbox = new List<Person>();
    private List<Person> personOutbox = new List<Person>();
    private int currentPersonIndex = -1;
    private Grades _grades;
    public TextMeshProUGUI labelName;
    [FormerlySerializedAs("inputNoteContent")] public TMP_InputField inputGradeContent;
    [FormerlySerializedAs("inputNoteScore")] public TMP_InputField inputGradeScore;
    
    void Awake()
    {
        studentData = FindObjectOfType<StudentData>();
    }

    public void Configure(Course course)
    {
        personInbox = new List<Person>(course.students);
        personOutbox = new List<Person>();
    }

    public void PickRandomNext()
    {
        // if there is a person already selected, save their changes in input fields
        if (currentPersonIndex >= 0)
        {
            SaveNoteForCurrentPerson();
        }
        // choose new person at random
        currentPersonIndex = Random.Range(0, personInbox.Count); 
        Person currentPerson = personInbox[currentPersonIndex];
        // show
        labelName.SetText(currentPerson.firstName + " " + currentPerson.lastName);
        // is there an existing entry for this?
        int noteEntryForThisPerson = GetGradeIndexOfPersonWithID(currentPersonIndex);
        if (noteEntryForThisPerson >= 0)
        {
            // if so let's pre-populate the text field data
            inputGradeContent.text = _grades.notes[noteEntryForThisPerson].noteContent;
            inputGradeScore.text = _grades.notes[noteEntryForThisPerson].noteScore.ToString();
        }
    }

    void SaveNoteForCurrentPerson()
    {
        Person currentPerson = personInbox[currentPersonIndex];
        personInbox.Remove(currentPerson);
        personOutbox.Add(currentPerson);
        // check if this person already has an entry in grades
        int noteEntryForThisPerson = GetGradeIndexOfPersonWithID(currentPersonIndex);
        // if it does exist...
        if (noteEntryForThisPerson >= 0)
        {
            // replace contents of this note with last edited contents in text fields
            _grades.notes[noteEntryForThisPerson] = new Grade(currentPerson.ID, int.Parse(inputGradeScore.text), inputGradeContent.text);
        }
        // if entry doesn't exist, make a new one
        else
        {
            _grades.notes.Add(new Grade(currentPerson.ID, int.Parse(inputGradeScore.text),
                inputGradeContent.text));
        }
        _grades.Save();
    }

    int GetGradeIndexOfPersonWithID(int id)
    {
        for (var index = 0; index < _grades.notes.Count; index++)
        {
            Grade n = _grades.notes[index];
            if (n.personID == id)
            {
                return n.personID;
            }
        }

        return -1;
    }
}

