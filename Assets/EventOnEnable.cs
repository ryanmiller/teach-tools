﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventOnEnable : MonoBehaviour
{
    public UnityEvent eventOnEnable;
    public UnityEvent eventOnDisable;

    void OnEnable()
    {
        eventOnEnable.Invoke();
    }

    void OnDisable()
    {
        eventOnDisable.Invoke();
    }
}
