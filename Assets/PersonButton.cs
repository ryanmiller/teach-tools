﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using TMPro.EditorUtilities;
using UnityEngine;

public class PersonButton : MonoBehaviour
{
    public TextMeshProUGUI personName;
    public TextMeshProUGUI personID;
    public Person myData;
    
    public void Initialize(Person personData)
    {
        myData = personData;
        personName.SetText(myData.firstName + " " + myData.lastName);
        personID.SetText(myData.ID.ToString());
        gameObject.SetActive(true);
    }

    private void OnDisable()
    {
        myData = null;
        personName.SetText("");
        personID.SetText("");
    }

    public void DeletePerson()
    {
        FindObjectOfType<StudentData>().RemovePersonFromCurrentCourse(myData);
    }
}