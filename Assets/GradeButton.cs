﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GradeButton : MonoBehaviour
{
    public TextMeshProUGUI title;
    private Grades myData;
    
    public void Initialize(Grades notes)
    {
        myData = notes;
        title.SetText(myData.noteTitle);
    }
}
