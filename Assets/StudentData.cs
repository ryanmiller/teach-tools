﻿using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.iOS;
using UnityEngine.Serialization;

public class StudentData : MonoBehaviour
{
    public TMPro.TMP_InputField fieldClassName;
    public TMPro.TMP_InputField fieldClassImport;
    [FormerlySerializedAs("thisCourse")] public Course importedCourse;
    
    public string CourseFolder()
    {
        return Application.persistentDataPath + Path.DirectorySeparatorChar + "courses" + Path.DirectorySeparatorChar;
    }

    public void ParseImportToCourse()
    {
        importedCourse = new Course();
        importedCourse.className = fieldClassName.text;
        string importContent = fieldClassImport.text;
        string[] importLines = importContent.Split('\n');
        for (int i = 0; i < importLines.Length; i++)
        {
            string[] separatingStrings = {"\t", " "};
            string[] lineContents = importLines[i].Split(separatingStrings, System.StringSplitOptions.RemoveEmptyEntries);
            if (lineContents.Length > 3)
            {
                int personID = -1;
                for (int j = 0; j < lineContents.Length; j++)
                {
                    int id;
                    if (int.TryParse(lineContents[j], out id))
                    {
                        personID = id;
                        break;
                    }
                }
                importedCourse.AddPerson(new Person(lineContents[0], lineContents[1], personID));
            }
        }
        fieldClassImport.text = "";
    }

    public GameObject warning;
    public TextMeshProUGUI warningText;

    public void ShowWarning(string message)
    {
        warningText.SetText(message);
        warning.SetActive(true);
    }
    
    public void SaveCourse()
    {
        if (string.IsNullOrEmpty(fieldClassName.text))
        {
            ShowWarning("You must name the course before you can save it.");
            return;
        }
        ParseImportToCourse();
        importedCourse.Save();
        ViewCourse(importedCourse);
    }

    public void SaveCourse(Course course)
    {
        string saveTo = CourseFolder() + course.className + ".json";
        string directory = Path.GetDirectoryName(saveTo);
        if (Directory.Exists(directory) == false)
        {
            Directory.CreateDirectory(directory);
        }
        string savedClass = JsonUtility.ToJson(course, true);
        File.WriteAllText(saveTo, savedClass);
        
    }

    public CourseButton courseButtonTemplate;
    public List<CourseButton> courseButtons = new List<CourseButton>();
    
    public void GetCourses()
    {
        string[] courseList = GetCourseList();
        foreach (CourseButton courseButton in courseButtons)
        {
            courseButton.gameObject.SetActive(false);
        }
        courseButtonTemplate.gameObject.SetActive(false);
        int courseButtonDeficit = courseList.Length - courseButtons.Count;
        // create more buttons?
        if (courseButtonDeficit > 0)
        {
            for (int a = 0; a < courseButtonDeficit; a++)
            {
                GameObject newCourseButton = (GameObject)Instantiate(courseButtonTemplate.gameObject, courseButtonTemplate.transform.parent);
                courseButtons.Add( newCourseButton.GetComponent<CourseButton>());
            }
        }
        // initialize buttons
        for (var index = 0; index < courseList.Length; index++)
        {
            Course courseData = JsonUtility.FromJson(File.ReadAllText(CourseFolder() + courseList[index] + ".json"),
                typeof(Course)) as Course;
            courseButtons[index].Initialize(courseData);
        }
        // disable extra buttons
        int extraButtons = courseButtons.Count - courseList.Length;
        if (extraButtons > 0)
        {
            for (int i = courseList.Length; i < courseButtons.Count; i++)
            {
                courseButtons[i].gameObject.SetActive(false);
            }
        }
    }
    
    public string[] GetCourseList()
    {
        DirectoryInfo dir = new DirectoryInfo(CourseFolder());
        FileInfo[] files = dir.GetFiles("*.json");
        List<string> fileNames = new List<string>();
        foreach (FileInfo file in files)
        {
            string displayName = file.Name.Substring(0, file.Name.IndexOf('.'));
            fileNames.Add(displayName);
        }
        return fileNames.ToArray();
    }

    public GameObject courseScreen;
    public PersonButton personButtonTemplate;
    public List<PersonButton> personButtons = new List<PersonButton>();
    public TextMeshProUGUI courseLabel;
    public Course currentViewedCourse;

    public void CloseCourse()
    {
        currentViewedCourse = null;
    }

    public void RemovePersonFromCurrentCourse(Person person)
    {
        if (currentViewedCourse == null)
        {
            ShowWarning("Error: Tried to remove " + person.firstName + " " + person.lastName + " from course, but the current course is not set. Please inform the developer, if possible.");
            return;
        }
        if (currentViewedCourse.ContainsPerson(person) == false)
        {
            ShowWarning("Error: Tried to remove " + person.firstName + " " + person.lastName + " from course, but this person is not a member of this course. Please inform the developer, if possible.");
            return;
        }
        currentViewedCourse.RemovePerson(person);
        importedCourse.Save();
        ViewCourse(importedCourse);
    }

    public void ViewCourse(Course course)
    {
        currentViewedCourse = course;
        FindObjectOfType<MainMenu>().OpenOnly(courseScreen);
        courseLabel.SetText(currentViewedCourse.className);
        PopulateCourseStudents();
        PopulateGrades();
    }
    
    private void PopulateCourseStudents(){
        // initialize the person buttons
        foreach (PersonButton personButton in personButtons)
        {
            personButton.gameObject.SetActive(false);
        }
        personButtonTemplate.gameObject.SetActive(false);
        int personButtonDeficit = currentViewedCourse.students.Count - personButtons.Count;
        // create more buttons?
        if (personButtonDeficit > 0)
        {
            for (int a = 0; a < personButtonDeficit; a++)
            {
                GameObject newPersonButton = (GameObject)Instantiate(personButtonTemplate.gameObject, personButtonTemplate.transform.parent);
                personButtons.Add( newPersonButton.GetComponent<PersonButton>());
            }
        }
        // initialize buttons
        for (var index = 0; index < currentViewedCourse.students.Count; index++)
        {
            personButtons[index].Initialize(currentViewedCourse.students[index]);
        }
        // disable extra buttons
        int extraButtons = personButtons.Count - currentViewedCourse.students.Count;
        if (extraButtons > 0)
        {
            for (int i = currentViewedCourse.students.Count; i < personButtons.Count; i++)
            {
                personButtons[i].gameObject.SetActive(false);
            }
        }
    }
    
    [FormerlySerializedAs("courseNoteButtonTemplate")] public GradeButton gradeButtonTemplate;
    [FormerlySerializedAs("courseNoteButtons")] public List<GradeButton> gradeButtons = new List<GradeButton>();
    [FormerlySerializedAs("newCourseNoteField")] public TMP_InputField newGradeField;
    [FormerlySerializedAs("currentCourseNotes")] public Grades currentGrades;
    [FormerlySerializedAs("newCourseNoteButton")] public Transform newGradeButton;
    
    public void AddNewGrade()
    {
        string gradeName = newGradeField.text;
        if (string.IsNullOrEmpty(gradeName))
        {
            ShowWarning("You need to enter a Course Note name.");
            return;
        }
        string gradePath = Application.persistentDataPath + Path.DirectorySeparatorChar + "courses" + Path.DirectorySeparatorChar + currentViewedCourse.className + Path.DirectorySeparatorChar;
        // if folder doesn't exist, make it 
        if (Directory.Exists(gradePath) == false)
        {
            Directory.CreateDirectory(gradePath);
        }
        // if folder does exist, ensure we aren't creating something that already exists
        else
        {
            if (File.Exists(gradePath + gradeName + ".json"))
            {
                ShowWarning("There are already course notes with the name '" + gradeName+ "'. Choose a different name.");
                return;
            }
        }
        // create it
        currentGrades = new Grades(currentViewedCourse, gradeName);
        currentGrades.Save();
        PopulateGrades();
        newGradeField.text = "";
    }
    
    private void PopulateGrades()
    {
        foreach (GradeButton gradeButton in gradeButtons)
        {
            gradeButton.gameObject.SetActive(false);
        }
        gradeButtonTemplate.gameObject.SetActive(false);
        // how many course notes?
        string gradePath = Application.persistentDataPath + Path.DirectorySeparatorChar + "courses" + Path.DirectorySeparatorChar + currentViewedCourse.className + Path.DirectorySeparatorChar;
        if (Directory.Exists(gradePath) == false)
        {
            Debug.Log("No Course Notes Directory for " + currentViewedCourse.className);
            return;
        }
        string[] gradeFiles = Directory.GetFiles(gradePath);
        if (gradeFiles.Length == 0)
        {
            Debug.Log("No Course Notes for " + currentViewedCourse.className);
            return;
        }
        int buttonDeficit = gradeFiles.Length - gradeButtons.Count;
        // create more buttons?
        if (buttonDeficit > 0)
        {
            for (int a = 0; a < buttonDeficit; a++)
            {
                GameObject newGradeButton = (GameObject)Instantiate(gradeButtonTemplate.gameObject, gradeButtonTemplate.transform.parent);
                gradeButtons.Add( newGradeButton.GetComponent<GradeButton>());
            }
        }
        // initialize buttons
        for (var index = 0; index < gradeFiles.Length; index++)
        {
            Grades loadedNotes = LoadGradesFromPath(gradeFiles[index]);
            if (loadedNotes != null)
            {
                gradeButtons[index].Initialize(loadedNotes);
                gradeButtons[index].gameObject.SetActive(true);
            }
        }
        // disable extra buttons
        int extraButtons = personButtons.Count - gradeFiles.Length;
        if (extraButtons > 0)
        {
            for (int i = gradeFiles.Length; i < gradeButtons.Count; i++)
            {
                gradeButtons[i].gameObject.SetActive(false);
            }
        }
        newGradeButton.SetAsLastSibling();
    }

    public Grades LoadGradesFromPath(string path)
    {
        string loadedText = File.ReadAllText(path);
        return JsonUtility.FromJson(loadedText, typeof(Grades)) as Grades;
    }

    public void DeleteCourse(string courseName)
    {
        string path = CourseFolder() + courseName;
        if (courseName.ToLower().Contains(".json") == false)
        {
            path += ".json";
        }
        if (File.Exists(path))
        {
            File.Delete(path);
        }
    }
}

[System.Serializable]
public class Course
{
    public List<Person> students = new List<Person>();
    public string className;

    public void GetDirectory()
    {
        
    }

    public string CourseFolder()
    {
        return Application.persistentDataPath + Path.DirectorySeparatorChar + "courses" + Path.DirectorySeparatorChar;
    }

    public void Save()
    {
        string saveTo = CourseFolder() + className + ".json";
        string directory = Path.GetDirectoryName(saveTo);
        if (Directory.Exists(directory) == false)
        {
            Directory.CreateDirectory(directory);
        }
        string savedClass = JsonUtility.ToJson(this, true);
        File.WriteAllText(saveTo, savedClass);
    }

    public void AddPerson(Person newPerson)
    {
        if (students.Contains(newPerson) == false)
        {
            students.Add(newPerson);
        }
    }

    public bool ContainsPerson(Person person)
    {
        foreach (Person p in students)
        {
            if (p.firstName.Equals(person.firstName) &&
                p.lastName.Equals(person.lastName) &&
                p.ID.Equals(person.ID))
            {
                return true;
            }
        }
        return false;
    }
    
    public void RemovePerson(Person person)
    {
        for (var index = 0; index < students.Count; index++)
        {
            Person p = students[index];
            if (p.firstName.Equals(person.firstName) &&
                p.lastName.Equals(person.lastName) &&
                p.ID.Equals(person.ID))
            {
                students.RemoveAt(index);
                Debug.Log("Removed " + person.ToString());
                return;
            }
        }
        Debug.LogWarning("Failed to remove " + person.ToString());
    }
}

[System.Serializable]
public class Person
{
    public Person(string firstName, string lastName, int id)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        ID = id;
    }

    public override string ToString()
    {
        return firstName + " " + lastName + " " + ID;
    }

    public string firstName;
    public string lastName;
    public int ID = 0;
}

[System.Serializable]
public class Grades
{
    public string noteTitle;
    public string className;
    public List<Grade> notes;

    public Grades(Course baseCourse, string noteTitle)
    {
        this.className = baseCourse.className;
        this.noteTitle = noteTitle;
        this.notes = new List<Grade>();
    }
    
    private string GradesFolder()
    {
        return Application.persistentDataPath + Path.DirectorySeparatorChar + "courses" + Path.DirectorySeparatorChar + className + Path.DirectorySeparatorChar;
    }
    
    public void Save()
    {
        string saveTo = GradesFolder() + noteTitle + ".json";
        string directory = Path.GetDirectoryName(saveTo);
        if (Directory.Exists(directory) == false)
        {
            Directory.CreateDirectory(directory);
        }
        string savedClass = JsonUtility.ToJson(this, true);
        File.WriteAllText(saveTo, savedClass);
    }
}

[System.Serializable]
public class Grade
{
    public int personID = -1;
    public int noteScore = 0;
    public string noteContent = "";

    public Grade(int personID = -1, int noteScore = 0, string noteContent = "")
    {
        this.personID = personID;
        this.noteScore = noteScore;
        this.noteContent = noteContent;
    }
}